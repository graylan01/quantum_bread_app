import React, { useEffect, useState } from 'react';
import { ActivityIndicator, View, Text, StyleSheet, TouchableOpacity, TextInput, Animated } from 'react-native'; // Import Animated from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from 'expo-linear-gradient';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SERVER_URL = 'http://localhost:8000';  // Replace with your FastAPI server URL

// Custom styles with animations
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabIcon: {
    marginBottom: -3,
  },
  tabBar: {
    backgroundColor: 'transparent',
    borderTopWidth: 0,
    elevation: 0,
  },
  settingsContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginBottom: 20,
    width: '100%',
  },
  settingsButton: {
    backgroundColor: '#ff1493',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  settingsButtonText: {
    color: '#fff',
    fontSize: 16,
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  animatedText: {
    fontSize: 24,
    color: '#ff1493',
    marginBottom: 20,
    opacity: 0,
  },
});

// Animated component for fading in text
const FadeInView = (props) => {
  const [fadeAnim] = useState(new Animated.Value(0));  // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 2000,  // Fade in duration
        useNativeDriver: true,
      }
    ).start();
  }, []);

  return (
    <Animated.View
      style={{
        ...props.style,
        opacity: fadeAnim,         // Bind opacity to animated value
      }}
    >
      {props.children}
    </Animated.View>
  );
};

// Components for each screen with animations
const SoilHealthScreen = () => (
  <View style={styles.container}>
    <FadeInView style={styles.animatedText}>
      <Text style={{ fontSize: 24 }}>Soil Health Screen</Text>
    </FadeInView>
  </View>
);

const EtherealInsightsScreen = () => (
  <View style={styles.container}>
    <FadeInView style={styles.animatedText}>
      <Text style={{ fontSize: 24 }}>Ethereal Insights Screen</Text>
    </FadeInView>
  </View>
);

const EconomicsScreen = () => (
  <View style={styles.container}>
    <FadeInView style={styles.animatedText}>
      <Text style={{ fontSize: 24 }}>Economics Screen</Text>
    </FadeInView>
  </View>
);

const CropSelectionScreen = () => (
  <View style={styles.container}>
    <FadeInView style={styles.animatedText}>
      <Text style={{ fontSize: 24 }}>Crop Selection Screen</Text>
    </FadeInView>
  </View>
);

const DetailedDataScreen = ({ route }) => {
  const { location, screenName } = route.params;
  const [detailedData, setDetailedData] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const endpoint = ${SERVER_URL}/fetch_detailed_data/${encodeURIComponent(screenName)};
      try {
        const response = await fetch(endpoint, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(location),
        });

        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }

        const data = await response.json();
        setDetailedData(data.detailed_data);
      } catch (error) {
        console.error('Error fetching data:', error.message);
        setError('Error fetching data. Please try again.');
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [location, screenName]);

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#ff1493" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {error ? (
        <Text style={{ color: '#ff6666', fontSize: 18 }}>{error}</Text>
      ) : (
        <FadeInView style={styles.animatedText}>
          <>
            <Text style={{ fontSize: 24, marginBottom: 20 }}>{screenName} Screen</Text>
            <Text style={{ fontSize: 18 }}>Detailed Data:</Text>
            <Text style={{ fontSize: 16 }}>{detailedData}</Text>
          </>
        </FadeInView>
      )}
    </View>
  );
};

const SettingsScreen = () => {
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');

  useEffect(() => {
    const fetchDefaultLocation = async () => {
      try {
        const savedLatitude = await AsyncStorage.getItem('defaultLatitude');
        const savedLongitude = await AsyncStorage.getItem('defaultLongitude');
        if (savedLatitude !== null && savedLongitude !== null) {
          setLatitude(savedLatitude);
          setLongitude(savedLongitude);
        }
      } catch (error) {
        console.error('Error fetching default location:', error.message);
      }
    };

    fetchDefaultLocation();
  }, []);

  const handleSave = async () => {
    try {
      await AsyncStorage.setItem('defaultLatitude', latitude);
      await AsyncStorage.setItem('defaultLongitude', longitude);
      console.log('Saved default latitude:', latitude);
      console.log('Saved default longitude:', longitude);
    } catch (error) {
      console.error('Error saving default location:', error.message);
    }
  };

  return (
    <View style={styles.settingsContainer}>
      <FadeInView style={styles.animatedText}>
        <Text style={{ fontSize: 24, marginBottom: 20 }}>Settings</Text>
      </FadeInView>
      <TextInput
        style={styles.input}
        placeholder="Default Latitude"
        value={latitude}
        onChangeText={setLatitude}
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        placeholder="Default Longitude"
        value={longitude}
        onChangeText={setLongitude}
        keyboardType="numeric"
      />
      <TouchableOpacity style={styles.settingsButton} onPress={handleSave}>
        <FadeInView style={styles.animatedText}>
          <Text style={styles.settingsButtonText}>Save</Text>
        </FadeInView>
      </TouchableOpacity>
    </View>
  );
};

const Tab = createBottomTabNavigator();

const App = () => (
  <LinearGradient colors={['#1a1a1a', '#2f2f2f']} style={{ flex: 1 }}>
    <NavigationContainer>
      <StatusBar barStyle="light-content" />
      <Tab.Navigator
        initialRouteName="SoilHealth"
        tabBarOptions={{
          style: styles.tabBar,
          activeTintColor: '#7efff5',
          inactiveTintColor: '#ccc',
        }}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'SoilHealth') {
              iconName = focused ? 'leaf' : 'leaf';
            } else if (route.name === 'EtherealInsights') {
              iconName = focused ? 'cloud' : 'cloud';
            } else if (route.name === 'Economics') {
              iconName = focused ? 'dollar' : 'dollar';
            } else if (route.name === 'CropSelection') {
              iconName = focused ? 'seedling' : 'seedling';
            } else if (route.name === 'Methods') {
              iconName = focused ? 'flask' : 'flask';
            } else if (route.name === 'MarketEtherealScanner') {
              iconName = focused ? 'chart-line' : 'chart-line';
            } else if (route.name === 'Settings') {
              iconName = focused ? 'cog' : 'cog';
            }

            return <FontAwesome5 name={iconName} size={size} color={color} style={styles.tabIcon} />;
          },
        })}
      >
        <Tab.Screen name="SoilHealth" component={SoilHealthScreen} />
        <Tab.Screen name="EtherealInsights" component={EtherealInsightsScreen} />
        <Tab.Screen name="Economics" component={EconomicsScreen} />
        <Tab.Screen name="CropSelection" component={CropSelectionScreen} />
        <Tab.Screen name="Methods" component={DetailedDataScreen} initialParams={{ screenName: 'Methods' }} />
        <Tab.Screen name="MarketEtherealScanner" component={DetailedDataScreen} initialParams={{ screenName: 'Market Ethereal Scanner' }} />
<Tab.Screen name="Settings" component={SettingsScreen} />
</Tab.Navigator>
</NavigationContainer>
</LinearGradient>
);

export default App;